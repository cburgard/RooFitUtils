#!/usr/bin/env python
import re
import sys

global printSisterNames
printSisterNames = False
global expandProducts
expandProducts = False
global ignoreComponents
global namemap
ignoreComponents = []
namemap = []
blacklist = []
blacklist_types = []

_files = []

tolerance = pow(10,-4)


colors = {
    "blue":'\033[94m',
    "green":'\033[92m',
    "yellow":'\033[93m',
    "red":'\033[91m',
    "bold":'\033[1m',
    "underline":'\033[4m',
}

def servers_helper(serverlist,pdf,expandproducts):
    for server in pdf.servers():
        if expandproducts and server.InheritsFrom(ROOT.RooProduct.Class()):
            servers_helper(serverlist,server,expandproducts)
        else:
            serverlist.add(server)

def keys(amap):
    return [ str(k) for k,v, in amap ]
            
def servers(pdf,expandproducts):
    serverlist = set()
    servers_helper(serverlist,pdf,expandproducts)
    return serverlist

def colored(s,c):
    return colors[c] + s + '\033[0m'


def weights(dh):
    return [ dh.weightArray()[i] for i in range(dh.arraySize()) ]

def eqHist(hf1,hf2, rel_tol):
    dh1 = hf1.dataHist()
    dh2 = hf2.dataHist()
    for a,b in zip(weights(dh1),weights(dh2)):
        if not isclose(a,b,rel_tol):
            return False
    return True

def isclose(a, b, rel_tol):
    return abs(a-b) <= rel_tol * max(abs(a), abs(b))

def notin(a,l):
    for x in l:
        if x.match(a):
            return False
    return True

def groupnames(m):
    return tuple( m.groupdict().keys() )

def namelist(args):
    retval = []
    if hasattr(args,"createIterator"):
      iter = args.createIterator()
    else:
      iter = args
    try:
        var = iter.Next()
        while var :
            retval.append(var.GetName())
            var = iter.Next()
    except AttributeError:
        for var in args: retval.append(var.GetName())
    return retval

def list_and(inlist):
    retval = True
    for x in inlist:
        retval = x and retval
    return retval

def makelist(args,type=None):
    retval = []

    if hasattr(args,"createIterator"):
        iter = args.createIterator()
    else:
        iter = args
    try:
        var = iter.Next()
        while var :
            if type and not var.InheritsFrom(type.Class()): continue
            retval.append(var)
            var = iter.Next()
    except AttributeError:
        for var in args:
            if type and not var.InheritsFrom(type.Class()): continue
            retval.append(var)
    return retval

def filter(inlist,whitelist,blacklist=[],blacklist_names=[]):
    return [ x for x in inlist if (any([x.InheritsFrom(y.Class()) for y in whitelist]) and not any([x.InheritsFrom(y.Class()) for y in blacklist]) and not any([ex.match(x.GetName()) for ex in blacklist_names])) ]

def binValues(dhist):
    values = []
    import ROOT
    for obs in makelist(dhist.get()):
        if obs.InheritsFrom(ROOT.RooRealVar.Class()):
            for i in range(0,obs.getBins()):
                x = dhist.get(i)
                values.append(dhist.weight())
        if obs.InheritsFrom(ROOT.RooAbsCategory.Class()):
            continue
    return values

def removeAll(s,l):
    mys = str(s)
    for x in l:
        mys = x.sub("",mys)
    return mys

# Optimized helper function to convert camelCase to snake_case
def camel_to_snake(s, exclusions=None):
    if exclusions is None:
        exclusions = set()  # Convert exclusions to a set for O(1) lookups

    # Build the result list
    result = []
    i = 0
    while i < len(s):
        # Check if the current substring matches any exclusion
        matched = False
        for exclusion in exclusions:
            if len(exclusion) < 2:
                continue
            if s.startswith(exclusion, i):
                # Handle prefixing and suffixing with "_"
                if not (i > 0 and s[i-1] == '_'):
                    result.append('_')
                result.append(exclusion)
                i += len(exclusion)
                if i < len(s) and s[i] != '_':
                    result.append('_')
                matched = True
                break

        if not matched:
            # Convert camelCase to snake_case
            if i > 0 and s[i].isupper() and (s[i-1].islower() or (i + 1 < len(s) and s[i+1].islower())):
                result.append('_')
            result.append(s[i].lower())
            i += 1
    
    return ''.join(result)

# Helper function to tokenize a snake_case string by underscores
def tokenize(s, ignored = []):
    if not isinstance(s, str):
        raise TypeError(f"Expected a string for tokenization, but got {type(s).__name__}.")

    # Convert camelCase to snake_case
    snake_case_string = camel_to_snake(s.replace(".","_"), sorted(ignored, key=lambda x: len(x), reverse=True))
    # Split by underscores and filter out any empty tokens
    tokens = snake_case_string.split('_')
    retval = set([token for token in tokens if len(token)>0 and (not ignored or not token in ignored)])
    return retval

# Updated compare function to use tokenization
def compare(first, second, ignoredFirst = ignoreComponents, ignoredSecond = ignoreComponents):   
    # Tokenize the resulting strings
    f_tokens = tokenize(first,ignoredFirst)
    s_tokens = tokenize(second,ignoredSecond)

    # If the tokenized lists match exactly, return True
    if f_tokens == s_tokens:
        return True

    return False

def compareNames(first, second, ignoreFirst = ignoreComponents, ignoreSecond = ignoreComponents):
    if (not first) and (not second):
        return True
    if (not first) or (not second):
        return False
    return compare(first.GetName(),second.GetName(), ignoreFirst, ignoreSecond)


def findByName(somelist,name):
    for obj in somelist:
        ma,mb = None,None
        for a,b in namemap:
            ma = a.match(obj.GetName())
            mb = b.match(name)
            if ma and mb: break
        if ma and mb:
            names = groupnames(ma)
            ok = True
            for x in names:
                if ma.group(x) != mb.group(x):
                    ok = False
            if ok:
                return obj
        if compare(obj.GetName(),name):
            return obj
    return None

def diffObjects(objtype,firstlabel,secondlabel,firstvars,secondvars):
    synced = synchronizeLists(
        filter(firstvars, [ROOT.TObject],[],blacklist),
        filter(secondvars, [ROOT.TObject],[],blacklist),            
        ignoreComponents,ignoreComponents)
    
    missing_in_first = []
    missing_in_second = []
    
    for first,second in synced:
        if first and not second:
            missing_in_second.extend(first)
        if second and not first:
            missing_in_first.extend(second)

    if len(missing_in_first)>0:
        print("the following "+objtype+" are missing in "+firstlabel+":")
        print(",".join(sorted(names(missing_in_first))))
            
    if len(missing_in_second)>0:
        print("the following "+objtype+" are missing in "+secondlabel+":")
        print(",".join(sorted(names(missing_in_second))))

    if not len(missing_in_first) and not len(missing_in_second):
        print("no difference in "+objtype)
        

def diffVarVariations(firstlabel,firstpdf,firstnset,secondlabel,secondpdf,secondnset,variation):
    print("name "+firstlabel+" "+secondlabel)
    pairs = []
    deltas = 0
    synced = synchronizeLists(
        filter(firstpdf.getVariables(), [ROOT.TObject],[],blacklist),
        filter(secondpdf.getVariables(), [ROOT.TObject],[],blacklist),            
        ignoreComponents,ignoreComponents)
    for f,s in synced:
        if not f or not s:
            continue
        for a,b in zip(f,s):
            if not variation:
                if abs(a.getErrorHi()) < tolerance and abs(b.getErrorHi()) < tolerance and abs(a.getErrorLo()) < tolerance and abs(b.getErrorLo()) < tolerance:
                    continue
            aval = a.getVal()
            bval = b.getVal()
            na = firstpdf.expectedEvents(firstnset)
            nb = secondpdf.expectedEvents(secondnset)
            if not variation:
                a.setVal(aval+abs(a.getErrorHi()))
                b.setVal(bval+abs(b.getErrorHi()))
            else:
                a.setVal(aval+variation)
                b.setVal(bval+variation)
            nahi = firstpdf.expectedEvents(firstnset)
            nbhi = secondpdf.expectedEvents(secondnset)
            if not variation:
                a.setVal(aval-abs(a.getErrorLo()))
                b.setVal(bval-abs(b.getErrorLo()))
            else:
                a.setVal(aval-variation)
                b.setVal(bval-variation)                
            nalo = firstpdf.expectedEvents(firstnset)
            nblo = secondpdf.expectedEvents(secondnset)
            a.setVal(aval)
            b.setVal(bval)
            if abs(nahi) < tolerance and abs(nalo) < tolerance and abs(nbhi) < tolerance and abs(nblo) < tolerance:
                continue
            if not isclose(na,nb,tolerance) or not isclose(nahi,nbhi,tolerance) or not isclose(nalo,nblo,tolerance):
                deltas = deltas + 1
                print("{:s}/{:s}:{:s}/{:s} {:f} {:f} {:f} : {:f} {:f} {:f}".format(firstpdf.GetName(),secondpdf.GetName(),a.GetName(),b.GetName(),na,nahi-na,nalo-na,nb,nbhi-nb,nblo-nb))
    if deltas < 1:
        print("no differences found")

def configureObservables(mc):
    if not mc.GetObservables():
        return
    for obs in mc.GetObservables():
        if obs.InheritsFrom(ROOT.RooRealVar.Class()):
            obs.setVal(obs.getMin())

def getHistFunc(datahist,ws):
    import ROOT
    obs = datahist.get().first()
    wsobs = ws.var(obs.GetName())
    if not wsobs or not wsobs.InheritsFrom(ROOT.RooRealVar.Class()): return None
    for obj in makelist(wsobs.clients()):
        if obj.InheritsFrom(ROOT.RooHistFunc.Class()):
            if obj.dataHist() == datahist: return obj
    return None

def getInterpolationVariable(hf):
    if not hf: return None
    for c in makelist(hf.clients()):
        if c.InheritsFrom(ROOT.PiecewiseInterpolation.Class()):
            for high,low,var in zip(makelist(c.highList()),makelist(c.lowList()),makelist(c.paramList())):
                if hf == high or hf == low:
                    return var
    return None

def longestCommonSubstring(s1, s2):
    m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]


def getSisterFunction(hf,varname):
    useHigh = False
    useLow = False
    highFunc = None
    lowFunc = None
    for c in makelist(hf.clientIterator()):
        if c.InheritsFrom(ROOT.PiecewiseInterpolation.Class()):
            for high,low,var in zip(makelist(c.highList()),makelist(c.lowList()),makelist(c.paramList())):
                if hf == high: useHigh=True
                elif hf == low: useLow=True
                if compare(var.GetName(),varname):
                    highFunc = high
                    lowFunc = low
    if useHigh: return highFunc
    elif useLow: return lowFunc
    return None



def diffFuncVals(firstlabel,firstfuncs,secondlabel,secondfuncs):
    print("name "+firstlabel+" "+secondlabel)
    for firstfunc in firstfuncs:
        secondfunc = findByName(secondfuncs,firstfunc.GetName())
        if secondfunc and not firstfunc.getVal() == secondfunc.getVal():
            print("{:s} {:f} {:f}".format(firstfunc.GetName(),firstfunc.getVal(),secondfunc.getVal()))

def diffDataVals(firstlabel,firstdata,secondlabel,seconddata,firstws,secondws):
    print("name "+firstlabel+" "+secondlabel)
    for firstd in firstdata:
        firsthf = getHistFunc(firstd,firstws)
        firstvar = getInterpolationVariable(firsthf)

        secondd = findByName(seconddata,firstd.GetName())
        if not secondd: continue

        secondhf = getHistFunc(secondd,secondws)
        secondvar = getInterpolationVariable(secondhf)
        if firstvar and secondvar and not compareNames(firstvar,secondvar):
            secondhf = getSisterFunction(secondhf,firstvar.GetName())
            secondd = secondhf.dataHist()

        firstvals = binValues(firstd)
        secondvals = binValues(secondd)

        equal = True
        for a,b in zip(firstvals,secondvals):
            if not a == b:
                equal = False
        if not equal:

            if firstvar:
                print(firstd.GetName()+"/"+secondd.GetName() + " ("+firstvar.GetName()+"/"+secondvar.GetName()+") " + " ".join(["{:f}/{:f}".format(a,b) for a,b in zip(firstvals,secondvals)]))
            else:
                print(firstd.GetName() + " " + " ".join(["{:f}/{:f}".format(a,b) for a,b in zip(firstvals,secondvals)]))


def getWS(filename):
    import ROOT
    rootfile = ROOT.TFile.Open(filename,"READ")
    _files.append(rootfile)
    for k in rootfile.GetListOfKeys():
        if k.GetClassName() == "RooWorkspace":
            return rootfile.Get(k.GetName())

def sortobjects(mylist):
    retval = {}
    for obj in mylist:
        if obj.ClassName() not in retval.keys(): retval[obj.ClassName()] = []
        retval[obj.ClassName()].append(obj)
        for k in retval.keys():
            retval[k] = sorted(retval[k],key = lambda x: str(x.GetName()).lower()+"_")
    return retval

def checkCompleteness(orig,sync):
    for o in orig:
        if not o: continue
        if not o in sync:
            raise RuntimeError("object '{:s}' is not in output list".format(o.GetName()))

def flatten(l):
    retval = []
    for x in l:
        retval.extend(x)
    return retval

def names(l):
    if not l: return []
    return [ x.GetName() for x in l ]
        
def synchronizeLists(first, second, ignoreFirst, ignoreSecond):
    # Returns a list of tuples of lists
    # Allows for one entry in 'first' to match multiple entries in 'second' and vice versa.

    by_token = {}
    for f in first:
        toks = tuple(sorted(list(tokenize(f.GetName(),ignoreFirst))))
        if not toks in by_token.keys():
            by_token[toks] = ([f],[])
        else:
            by_token[toks][0].append(f)

    for s in second:
        toks = tuple(sorted(list(tokenize(s.GetName(),ignoreSecond))))
        if not toks in by_token.keys():
            by_token[toks] = ([],[s])
        else:
            by_token[toks][1].append(s)

#    for k,v in by_token.items():
#        print(k,names(v[0]),names(v[1]))
            
    matched = []
    unmatched_f = []
    unmatched_s = []    
    for k,v in by_token.items():
        if all(v):
            matched.append(v)
        elif v[0]:
            unmatched_f.append(v[0])
        elif v[1]:
            unmatched_s.append(v[1])            

    to_match_f = flatten(unmatched_f)
    to_match_s = flatten(unmatched_s)

    matched_s = set()
    unmatched_f_final = []
    
    for f in to_match_f:
        if f.InheritsFrom(ROOT.RooHistFunc.Class()):
            found = None
            for s in to_match_s:
                if s.InheritsFrom(ROOT.RooHistFunc.Class()) and eqHist(f, s, tolerance) and s not in matched_s:
                    found = s
                    matched.append(([f], [s]))
                    matched_s.add(s)
                    break
            if not found:
                unmatched_f_final.append(f)
        else:
            unmatched_f_final.append(f)
    
    unmatched_s_final = [s for s in to_match_s if s not in matched_s]

    return matched + [ ([f],None) for f in unmatched_f_final ] + [ (None,[s]) for s in unmatched_s_final ]

import numpy as np
from itertools import permutations
from Levenshtein import distance as levenshtein_distance

def optimal_string_pairing(list1, list2):
    # Ensure the lists are of the same length
    if len(list1) != len(list2):
        raise ValueError("Both lists must be of the same length")
    
    # Create copies of the lists to work on
    list1 = list1[:]
    list2 = list2[:]
    
    # Step 1: Pair identical strings
    paired_list = []
    list1_remainder = []
    list2_remainder = []
    
    for string in list1:
        if string in list2:
            paired_list.append((string, string))
            list2.remove(string)
        else:
            list1_remainder.append(string)
    
    list2_remainder = list2  # Remaining strings in list2
    
    # Step 2: Use more complex computation for the remaining strings
    n = len(list1_remainder)
    
    if n > 0:
        distance_matrix = np.zeros((n, n))
        for i in range(n):
            for j in range(n):
                distance_matrix[i, j] = levenshtein_distance(list1_remainder[i], list2_remainder[j])
        
        # Find the optimal pairing using the Hungarian algorithm
        from scipy.optimize import linear_sum_assignment
        row_ind, col_ind = linear_sum_assignment(distance_matrix)
        
        for i in range(n):
            paired_list.append((list1_remainder[row_ind[i]], list2_remainder[col_ind[i]]))
    
    return paired_list

def diffPdfTrees(firstpdfs,secondpdfs,indent=0):
    global ignoreParentComponents    
    global printSisterNames
    global expandProducts
    
    myindent = indent+1
    if firstpdfs:
        firstcomps  = flatten(servers(pdf,pdf.InheritsFrom(ROOT.RooProduct.Class()) and expandProducts) for pdf in firstpdfs)
    else:
        firstcomps = []
    if secondpdfs:
        secondcomps  = flatten(servers(pdf,pdf.InheritsFrom(ROOT.RooProduct.Class()) and expandProducts) for pdf in secondpdfs)        
    else:
        secondcomps = []

    if not firstcomps or not secondcomps:
        return

    ignored_first = set(ignoreComponents)
    ignored_second = set(ignoreComponents)
    if ignoreParentComponents:
        for pdf in firstpdfs:
            ignored_first.update(tokenize(pdf.GetName()))
        for pdf in secondpdfs:
            ignored_second.update(tokenize(pdf.GetName()))        
        
    firstfuncs  = sortobjects(filter(set(firstcomps), [ROOT.RooAbsReal],blacklist_types,blacklist))
    secondfuncs = sortobjects(filter(set(secondcomps),[ROOT.RooAbsReal],blacklist_types,blacklist))

    classes = set(firstfuncs.keys())
    classes.update(secondfuncs.keys())
    synced = {}
    for c in classes:
        if (c in firstfuncs) and (c in secondfuncs):
            synced[c] = synchronizeLists(firstfuncs[c],secondfuncs[c], ignored_first, ignored_second)
        elif c in secondfuncs:
            synced[c] = [(None,[x]) for x in secondfuncs[c]]
        elif c in firstfuncs:
            synced[c] = [([x],None) for x in firstfuncs[c]]            
            
    prefix = "  "*(myindent)
    for c in sorted(classes):
        for first,second in synced[c]:
            ok = True
            if first:
                firstname =  ",".join([f.GetName() for f in first])
            else:
                firstname = "(None)"
                ok = False
            if second:
                secondname = ",".join([s.GetName() for s in second])
            else:
                secondname = "(None)"
                ok = False
            if printSisterNames:
                names = firstname + "/" + secondname + " (" + c + ")"
            else:
                names = firstname + " (" + c + ")"
            if not ok:
                print(prefix+colored(names,"red"))
            else:
                firstnumbers = ",".join([str(f.getVal()) for f in first])
                secondnumbers = ",".join([str(s.getVal()) for s in second])
                numbers = "["+firstnumbers+"/"+secondnumbers+"]"
                if firstnumbers == secondnumbers:
                    print(prefix+names+" "+numbers)
                else:
                    print(prefix+names+" "+colored(numbers,"yellow"))                    
                diffPdfTrees(first,second,myindent)
                   
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser("wsdiff",description="investigate differences between workspaces")
    parser.add_argument("first",type=str,help="first workspace")
    parser.add_argument("second",type=str,help="second workspace")
    parser.add_argument("--first-snapshot", "-s1",dest="s1",type=str,help="first snapshot")
    parser.add_argument("--second-snapshot","-s2",dest="s2",type=str,help="second workspace")
    parser.add_argument("--ignore",nargs="+",type=str,default=["auto","alpha","Constraint$","constraint$"],help="ignore certain name components")
    parser.add_argument("--print-sister-names",action="store_true", default=False, help="print both names for matched comparisons")
    parser.add_argument("--ignore-parent-components",action="store_true", default=False, help="automatically ignore all compontents that are part of parent names")    
    parser.add_argument("--resolve-nested-products",action="store_true", default=False, help="resolve products of products and merge the lists of components")    
    parser.add_argument("--blacklist-names",nargs="+",type=str,default=["nom_.*"],help="do not show certain objects (by name)")
    parser.add_argument("--blacklist-types",nargs="+",type=str,default=["RooConstVar"],help="do not show certain objects (by type)")
    parser.add_argument("--map",nargs="+",type=str,help="map certain names",default=[])
    parser.add_argument('--models',                           dest='models', action='store_true'   , help="show models", default=True )
    parser.add_argument('--no-models',                        dest='models', action='store_false'  , help="do not show models", default=True )
    parser.add_argument('--tree',                           dest='tree', action='store_true'   , help="show tree", default=True )
    parser.add_argument('--no-tree',                        dest='tree', action='store_false'  , help="do not show tree", default=True )
    parser.add_argument('--variables',                           dest='vars', action='store_true'   , help="show variables", default=True )
    parser.add_argument('--no-variables',                        dest='vars', action='store_false'  , help="do not show variables", default=True )
    parser.add_argument('--variations',                           dest='variations', action='store_true'   , help="show variations", default=True )
    parser.add_argument('--no-variations',                        dest='variations', action='store_false'  , help="do not show variations", default=True )
    parser.add_argument('--dynamic-variations',                           dest='dynamic_variations', action='store_true'   , help="use dynamic variations (errors of parameters)", default=True )
    parser.add_argument('--static-variations',                        dest='dynamic_variations', action='store_false'  , help="use statis variations", default=True )    
    parser.add_argument('--functions',                           dest='funcs', action='store_true'   , help="show functions", default=True )
    parser.add_argument('--no-functions',                        dest='funcs', action='store_false'  , help="do not show functions", default=True )
    parser.add_argument('--data',                           dest='data', action='store_true'   , help="show data", default=True )
    parser.add_argument('--no-data',                        dest='data', action='store_false'  , help="do not show data", default=True )
    parser.add_argument('--all',                           dest='all', action='store_true'   , help="show list of all objects", default=True )
    parser.add_argument('--no-all',                        dest='all', action='store_false'  , help="do not show list of all objects", default=True )
    parser.add_argument('--tolerance',                      dest='tolerance', type=int, default=4  , help="number of digits in precision to compare" )
    parser.add_argument('--why',                      dest='why', type=str, default=None, nargs=2, help="debugging option: instead of running the comparison, show why two strings names do not match given the current options")
    args = parser.parse_args()

    if args.why:
        print(sorted(list(tokenize(args.why[0],args.ignore))))
        print(sorted(list(tokenize(args.why[1],args.ignore))))        
        exit(0)

    tolerance = pow(10,-args.tolerance)
        
    first = getWS(args.first)
    first.SetTitle(args.first)
    if args.s1:
        first.loadSnapshot(args.s1)

    second = getWS(args.second)
    second.SetTitle(args.second)
    if args.s2:
        second.loadSnapshot(args.s2)
        
    import re
    import ROOT
    ignoreComponents = set(args.ignore)
    blacklist = [ re.compile(x) for x in args.blacklist_names ]
    blacklist_types = [ getattr(ROOT,t) for t in args.blacklist_types ]    
    printSisterNames = args.print_sister_names
    ignoreParentComponents = args.ignore_parent_components
    expandProducts = args.resolve_nested_products    

    for mapfile in args.map:
        with open(mapfile,"r") as infile:
            for line in infile:
                if len(line.strip()) == 0: continue
                if line.startswith("#"):
                    continue
                bits = line.split()
                if not len(bits) == 2:
                    print("unable to read line :"+line)
                    continue
                a = re.compile(bits[0])
                b = re.compile(bits[1])
                namemap.append((a,b))
                namemap.append((b,a))

    import ROOT
    ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.FATAL)

    firstmodels = makelist(first.allGenericObjects(),ROOT.RooStats.ModelConfig)
    secondmodels = makelist(second.allGenericObjects(),ROOT.RooStats.ModelConfig)

    if args.models:
        print("== models ==")
        diffObjects("Models",first.GetTitle(),second.GetTitle(),firstmodels,secondmodels)
        print("")
        for firstmc,secondmc in zip(firstmodels,secondmodels):
            configureObservables(firstmc)
            configureObservables(secondmc)

            if args.tree:
                diffPdfTrees([firstmc.GetPdf()],[secondmc.GetPdf()])

            print("POIs:")
            firstpois = makelist(firstmc.GetParametersOfInterest())
            secondpois = makelist(secondmc.GetParametersOfInterest())
            diffObjects("POIs",first.GetTitle(),second.GetTitle(),firstpois,secondpois)
            print("NPs:")
            firstnps = makelist(firstmc.GetNuisanceParameters())
            secondnps = makelist(secondmc.GetNuisanceParameters())
            diffObjects("NPs",first.GetTitle(),second.GetTitle(),firstnps,secondnps)
            print("Obs.:")
            firstobs = makelist(firstmc.GetObservables())
            secondobs = makelist(secondmc.GetObservables())
            diffObjects("Obs",first.GetTitle(),second.GetTitle(),firstobs,secondobs)

    if args.vars:
        print("\n\n== variables ==")
        firstvars = makelist(first.allVars())
        secondvars = makelist(second.allVars())
        diffObjects("Variables",first.GetTitle(),second.GetTitle(),secondvars,firstvars)
        print("")
        for vf in firstvars:
            for firstmc in firstmodels:
                if firstmc.GetObservables().find(vf):
                    vf = None
                    break
            if not vf:
                continue
            vs = second.var(vf.GetName())
            if vs:
                valf, vals = vf.getVal(),vs.getVal()
                if valf != vals:
                    print(vf.GetName(),vf.isConstant(),vals.isConstant(),valf,vals)
        if args.variations:
            variation = 1
            if args.dynamic_variations:
                variation = None
            for firstmc,secondmc in zip(firstmodels,secondmodels):
                firstpdf = firstmc.GetPdf()
                secondpdf = secondmc.GetPdf()
                if firstpdf.InheritsFrom(ROOT.RooSimultaneous.Class()) and secondpdf.InheritsFrom(ROOT.RooSimultaneous.Class()):
                    firstcats = set(keys(firstpdf.indexCat().states()))
                    secondcats = set(keys(secondpdf.indexCat().states()))
                    categories = optimal_string_pairing(list(firstcats),list(secondcats))
                    for firstcat, secondcat in categories:
                        print("\n= category: " + firstcat + "/" + secondcat + " =")
                        diffVarVariations(first.GetTitle(),firstpdf.getPdf(firstcat),firstmc.GetObservables(),second.GetTitle(),secondpdf.getPdf(secondcat),secondmc.GetObservables(),variation)
                else:
                    diffVarVariations(first.GetTitle(),firstmc.GetPdf(),firstmc.GetObservables(),second.GetTitle(),secondmc.GetPdf(),secondmc.GetObservables(),variation)

    if args.funcs:
        print("\n\n== functions ==")
        firstfuncs = makelist(first.allFunctions())
        secondfuncs = makelist(second.allFunctions())
        diffObjects("Functions",first.GetTitle(),second.GetTitle(),firstfuncs,secondfuncs)
        print("")
        diffFuncVals(first.GetTitle(),firstfuncs,second.GetTitle(),secondfuncs)

    if args.data:
        print("\n\n== datasets ==")
        firstdata = first.allData()
        seconddata = second.allData()
        diffObjects("Datasets",first.GetTitle(),second.GetTitle(),firstdata,seconddata)
        print("")
        diffDataVals(first.GetTitle(),firstdata,second.GetTitle(),seconddata,first,second)

    if args.all:
        print("\n\n== generic objects ==")
        firstfuncs = makelist(first.allGenericObjects())
        secondfuncs = makelist(second.allGenericObjects())
        diffObjects("Objects",first.GetTitle(),second.GetTitle(),firstfuncs,secondfuncs)


