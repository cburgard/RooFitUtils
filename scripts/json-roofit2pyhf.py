#!/bin/env python

from math import sqrt

def stripname(name,stripcomponents):
    while True:
        anymatch = False
        for c in stripcomponents:
            if name.endswith(c):
                name = name[:-len(c)].strip("_")
                anymatch = True
                break
            if name.startswith(c):
                name = name[len(c):].strip("_")
                anymatch = True
                break
            if c in name:
                name = name.replace("_"+c+"_","_")
                anymatch = True
                break
        if not anymatch:
            return name

def main(args):
    import re
    import json
    
    # read the input
    with open(args.input,"rt") as input:
        input_json = json.load(input)


    variables = input_json.get("variables",{})
    bounds = {}
    for domain in input_json.get("domains",[]):
        for parameter in domain["axes"]:
            bounds[parameter["name"]] = [parameter["min"],parameter["max"]]

    # setup the main structure    
    pois = set()
    for analysis in input_json["analyses"]:
        for poi in analysis["parameters_of_interest"]:
            pois.add(poi)
    measurement = {"name":"meas","config":{"parameters":[]}}
    if args.poi:
        measurement["config"]["poi"] = args.poi
    elif len(pois) > 0:
        measurement["config"]["poi"] = list(pois)[0]
    output_json = {"channels":[],"measurements":[measurement],"observations":[],"version":"1.0.0"}

    # some bookkeeping
    nps = set()
    nfs = set()    
    channelnames = []

    # define observations / data
    data = {}
    for key,channel in data.items():
        if type(channel) != dict:
            continue
        channelname = stripname(key,args.strip_name_components)
        channelnames.append(channelname)
        if "counts" in channel.keys():
            output_json["observations"].append({"data":channel["counts"],"name":channelname})        
        else:
            output_json["observations"].append({"data":channel["weights"],"name":channelname})

    observations = []
    parameters = {}
            
    # define model /pdf
    for pdf in sorted(input_json.get("distributions",[]),key=lambda x:x["name"]):
        if pdf["type"] != "histfactory_dist":
            continue
        if "name" in pdf.keys(): key = pdf["name"]
        channelname = stripname(key,args.strip_name_components)
        for c in channelnames:
            if c.startswith(channelname):
                channelname = c

        for any_data in input_json["data"]:
            if args.data in any_data["name"] and channelname in any_data["name"]:
                observations.append({"data":any_data["contents"], "name":channelname})

        out_channel = {"name":channelname,"samples":[]}
        output_json["channels"].append(out_channel)

        if not "samples" in pdf.keys():
            print(args.input + " is no histfactory workspace")
            exit(1)

        sum_values = None
        sum_errors2 = None
        
        for sample in pdf["samples"]:
            if not "data" in sample.keys():
                print(args.input + " no histfactory workspace")
                exit(1)
            has_staterror = False
            for modifier in sample["modifiers"]:
                if modifier["type"] == "staterror":
                    has_staterror = True

            if has_staterror:
                values = sample["data"]["contents"]
                if sum_values:
                    for i in range(len(sum_values)):
                        sum_values[i] += values[i]
                else:
                    sum_values = [ v for v in values ]
                    
                errors = sample["data"]["errors"]
                if sum_errors2:
                    for i in range(len(sum_errors2)):
                        sum_errors2[i] += errors[i]*errors[i]
                else:
                    sum_errors2 = [ e*e for e in errors ]            
            
        for sample in sorted(pdf["samples"],key=lambda x:x["name"]):
            values = sample["data"]["contents"]
            bins = len(values)
            
            out_sample = {"name":stripname(sample["name"],args.strip_name_components),"modifiers":[]}
            out_channel["samples"].append(out_sample)
            out_sample["data"] = values

            modifiers = out_sample["modifiers"]
            for modifier in sorted(sample["modifiers"],key=lambda x:x["name"]):
                if modifier.get("constraint",None) == "Const":
                    continue
                if modifier["name"] == "Lumi":
                    modifiers.append({"name": "lumi", "type": "lumi", "data": None})
                    nps.add("lumi")
                    parameters["lumi"] = {"fixed":False,"inits":[1.],"auxdata":[1.],"bounds":[bounds[modifier["parameter"]]]}
                elif modifier["type"] == "staterror":
                    parname = "staterror_"+channelname
                    modifiers.append({"name": parname, "type":"staterror", "data" : [ sqrt(sum_errors2[i])/sum_values[i] * values[i] for i in range(bins) ]})
                    nps.add(parname)
                    parameters[parname] = {"fixed":False,"auxdata":[1. for i in range(bins)],"inits":[1. for i in range(bins)],"bounds":[[-5.,5.] for i in range(bins)]}
                elif modifier["type"] == "normfactor":
                    modifiers.append({"name": modifier["parameter"], "type": "normfactor", "data":None})
                    nfs.add(modifier["parameter"])
                    parameters[modifier["parameter"]] = {"fixed":False,"inits":[1.],"bounds":[bounds[modifier["parameter"]]]}
                elif modifier["type"] == "normsys":
                    modifiers.append({"name": modifier["parameter"], "type": "normsys", "data":modifier["data"]})
                    nps.add(modifier["parameter"])
                    parameters[modifier["parameter"]] = {"fixed":False,"auxdata":[0.],"inits":[0.],"bounds":[bounds[modifier["parameter"]]]}
                elif modifier["type"] == "shapesys":
                    parname = modifier["name"]
                    nps.add(parname)
                    modifiers.append({"name": parname, "type": "shapesys", "data": modifier["data"]["vals"]})
                    parameters[parname] = {"fixed":False,"auxdata":[1. for i in range(bins)],"inits":[0. for i in range(bins)],"bounds":[[-5.,5.] for i in range(bins)]}
                    if bins == 9:
                        print(parname)
                elif modifier["type"] == "histosys":
                    modifiers.append({"name": modifier["parameter"], "type": "histosys", "data": {"hi_data":modifier["data"]["hi"]["contents"],"lo_data":modifier["data"]["lo"]["contents"]}})
                    nps.add(modifier["parameter"])
                    parameters[modifier["parameter"]] = {"fixed":False,"auxdata":[0.],"inits":[0.],"bounds":[bounds[modifier["parameter"]]]}
                else:
                    print(args.input + " contains unknown modifier type")                    
                    exit(1)

    # define parameters
    for par in nps:
        parameters[par]["name"]=par
        measurement["config"]["parameters"].append(parameters[par])
    for par in nfs:
        if any([ re.match(b,par) for b in args.nf_blacklist]): continue
        parameters[par]["name"]=par
        measurement["config"]["parameters"].append(parameters[par])        

    # some post-processing
    for p in measurement["config"]["parameters"]:
        pname = p["name"]
        if pname in variables.keys():
            if "const" in variables[pname].keys():
                p["fixed"] = variables[pname]["const"]
        if args.fix_other_pois:
            for poi in pois:
                if poi == args.poi: continue
                if pname == poi:
                    p["fixed"] = True

    # write observations
    output_json["observations"] = observations

    # write output
    with open(args.output,"wt") as output:
        json.dump(output_json,output,sort_keys=True)
                
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="format converter between ROOT JSON and pyhf JSON")
    parser.add_argument("--poi",default=None)
    parser.add_argument("--data",default="obsData",help="name of the dataset")
    parser.add_argument("--fix-other-pois",action="store_true")
    parser.add_argument("--nf-blacklist",nargs="+",default=["binWidth_.*","one"],dest="nf_blacklist")
    parser.add_argument("--sys-blacklist",nargs="+",default=["zero"],dest="sys_blacklist")    
    parser.add_argument("--strip-name-components",nargs="+",default=["model"])
    parser.add_argument("--defactorize",action="store_true",help="merge OverallSys and HisotSys of the same name",default=False)
    parser.add_argument("input",help="RooFit JSON file")
    parser.add_argument("output",help="pyhf JSON output")
    args = parser.parse_args()
    main(args)
    
    
